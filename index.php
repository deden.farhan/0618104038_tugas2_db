<p>Deden Farhan <br></p>
<p>0618104038 <br></p>

<?php 
	include 'connect.php';
    
    function addMahasiswa($npm, $nama_mhs, $alamat, $no_tel){
    	$sql = $conn->prepare("INSERT INTO t_mahasiswa (npm, nama_mhs, alamat, no_tel) VALUES (:npm, :nama_mhs, :alamat, :no_tel)");
	    $sql->bindParam(':npm', $npm);
	    $sql->bindParam(':nama_mhs', $nama_mhs);
	    $sql->bindParam(':alamat', $alamat);
	    $sql->bindParam(':no_tel', $no_tel);
    	$sql->execute();
    	$conn->close();
    }


    function getvMahasiswa(){
    	$sql = $conn->prepare("SELECT * FROM t_mahasiswa");
    	$sql->execute();
    	$mahasiswa = $sql->fetchAll();

    	echo "<table border='1'>";

	    foreach ($mahasiswa as $v) {
		    echo 	"<tr>
		    			<td>".$v['npm']."</td>
		    			<td>".$v['nama_mhs']."</td>
		    			<td>".$v['alamat']."</td>
		    			<td>".$v['no_tel']."</td>
		    		</tr>";
		}

		echo "</table>";
    }

    function updateMahasiswa($npm, $nama_mhs, $alamat, $no_tel){
    	try {
    		$sql = "UPDATE t_mahasiswa SET nama_mhs='$nama_mhs', alamat='$alamat', no_tel='$no_tel' WHERE npm=$npm";
    		$stmt = $conn->prepare($sql);
    		$stmt->execute();
    		$no=$stmt->rowCount();
			echo " No of records = ".$no;  
    	} catch(PDOException $e){
    		echo $sql . "<br>" . $e->getMessage();
    	}
    }

    function deleteMahasiswa($npm){
    	try {
    		$sql = "DELETE FROM t_mahasiswa WHERE npm=$npm";
    		$stmt = $conn->prepare($sql);
    		$stmt->execute();
    	} catch(PDOException $e){
    		echo $sql . "<br>" . $e->getMessage();
    	}
    }

    function cekvAvailable($npm){
    	try {
    		$sql = "SELECT npm FROM t_mahasiswa WHERE npm=$npm";
    		$stmt = $conn->prepare($sql);
    		$stmt->execute();

    		$row = $stmt->rowCount();
    		$return = ($row == 1) ? true : false;
    		if (!$return)
    			echo "v Tidak Tersedia";
    		
    		return $return;
    	} catch(PDOException $e){
    		echo $sql . "<br>" . $e->getMessage();
    	}

    	return false;
    }

    getvMahasiswa();
?>